﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameController : MonoBehaviour {
    public Text ordertext;
    public int strikes = 0;
    public AudioSource source;
    private float volLowRange = .5f;
    private float volHighRange = 1.0f;
    public Image imgLife1, imgLife2, imgLife3;

 
    //buttons
    public bool shaken = false;
    public int bitters = 0;
    public int rum = 0;
    public int brandy = 0;
    public int gin = 0;
    public bool ice = false;
    public bool wedge = false;
    public bool stirred = false;
    //orders
    public int oBitters;
    public int oRum;
    public int oGin;
    public bool oShaken;
    public bool oIce;
    public bool oWedge;
    public bool oStirred;
    public int oAge;
    public bool oFace = false;
    public bool oBar = true;
    public int oBrandy;

    // Use this for initialization
    void Start() {
        source = GetComponent<AudioSource>();
        Reset();

    }
    void Giveorder()
    {
        if (Random.value >= .05f)
        {
            oBar = false;
        }
        
        if (Random.value >= .5f)
        {
            oShaken = true;
        }
        else
        {
            oShaken = false;
        }

        if (Random.value >= .25f)
        {
            oWedge = true;
        }
        else
        {
            oWedge = false;
        }
        if (Random.value >= .3f)
        {
            oIce = true;
        }

        else
        {
            oIce = false;
        }
        if (Random.value >= .12f)
        {
            oStirred = true;
        }
        else
        {
            oStirred = false;
        }
        if (Random.value >= .5f)
        {
            oFace = true;
        }
        oBitters = Random.Range(0, 4);
        oRum = Random.Range(0, 6);
        oGin = Random.Range(0, 3);
        oAge = Random.Range(7, 100);
        oBrandy = Random.Range(0, 5);
        //the order text

        if (strikes >= 3)
        {
            ordertext.text = "Game Over! You're fired...";
            strikes = 0;
            imgLife1.enabled = false;
            imgLife2.enabled = false;
            imgLife3.enabled = false;


            Invoke("Reset", 2);
        }
        
        string order = "Hey give me a-uhm ";
        if (oBitters != 0)
        {
            order += oBitters.ToString() + " shots of bitters, ";
        }
        if (oRum != 0)
        {
            order += oRum.ToString() + " shots of rum, ";
        }
        if (oGin != 0)
        {
            order += oGin.ToString() + " shots of Gin, ";

        }
        if (oBrandy != 0)
        {
            order += oBrandy.ToString() + " shots of Brandy, ";
        }
        if (oStirred == false && oShaken == true)
        {
            order += " shaken not stirred,";
        }
        if (oShaken == true && oStirred == true)
        {
            order += " shaken,";
        }



        if (oStirred == true)
        {
            order += " stirred,";
        }
        if (oIce == true)
        {
            order += " on the rocks.";
        }

        if (oWedge == true)
        {
            order += " With a lemon wedge.";
        }

        if (oBar == false)
        {
            order += " Oh and your bar is a mess.";
        }

       
        ordertext.text = order;
        CancelInvoke("TimesUp");
        Invoke("TimesUp", 23);

    }

    // check the order
    public void Serve()
    {
        if (shaken != oShaken) {
            Fail();
            return;
        }

        if (bitters != oBitters)
        {
            Fail();
            return;
        }

        if (ice != oIce)
        {
            Fail();
            return;
        }

        if (rum != oRum)
        {
            Fail();
            return;
        }
        if (gin != oGin) {
            Fail();
            return;
        }
        if (stirred != oStirred)
        {
            Fail();
            return;
        }
        if (wedge != oWedge)
        {
            Fail();
            return;
        }
        if (oAge <= 20)
        {
            IdFail();
        }

        if (oFace == true)
        {
            IdFail();
        }

        if (oBar == false)
        {
            CleanFail();
        }       
        ordertext.text = "Now that's good!";
        Invoke("Reset", 2);
    }


    private void TimesUp()
    {
        ordertext.text = "Times up, they walked away!";
        strikes++;
        Invoke("Reset", 2);
    }
    private void Fail() {
       

        
        
            ordertext.text = "Gross!";
            strikes++;
            
        if (strikes==1)
            imgLife1.enabled = true;

        if (strikes <= 2)
            imgLife2.enabled = true;

        if (strikes <= 3)
            imgLife3.enabled = true;
        if (oAge <= 20)
        {
            IdFail();
        }

        if (oFace == true)
        {
            IdFail();
        }

        if (strikes >= 3)
        {
            ordertext.text = "Game Over! You're fired...";
            strikes = 0;
            imgLife1.enabled = false;
            imgLife2.enabled = false;
            imgLife3.enabled = false;
        }
        Invoke("Reset", 2);

    }

    private void IdFail()
    {

        ordertext.text += "The customer was also not legal too.";
        strikes++;
        if (strikes == 1)
            imgLife1.enabled = true;

        if (strikes <= 2)
            imgLife2.enabled = true;

        if (strikes <= 3)
            imgLife3.enabled = true;
        Invoke("Reset", 2);
    }

    private void CleanFail()
    {

        ordertext.text += "Bar was too nasty.";
        strikes++;
        if (strikes >= 1)
            imgLife1.enabled = true;

        if (strikes <= 2)
            imgLife2.enabled = true;

        if (strikes <= 3)
            imgLife3.enabled = true;
        Invoke("Reset", 2);
    }
    private void Reset()
    {
        shaken = false;
        bitters = 0;
        rum = 0;
        gin = 0;
        ice = false;
        wedge = false;
        stirred = false;
        oFace = false;
        oBar = true;
        brandy = 0;
        Giveorder();
    }
    // Update is called once per frame
    void Update() {

    }


    // the drinks buttons
    public void Shake()
    {
        shaken = true;

    }

    public void Bitters()
    {
        bitters += 1;
    }

    public void Ice()
    {
        ice = true;
    }

    public void Gin()
    {
        gin += 1;

    }

    public void Rum()
    {
        rum += 1;
    }

    public void Wedge()
    {
        wedge = true;
    }

    public void Stirred()
    {
        stirred = true;
    }

    public void Age()
    {
        ordertext.text += " Their age is, " + oAge;
    }
    public void Face()
    {
        if (oFace == true)
            ordertext.text += " They look nothing like the person on the ID";
        else
            ordertext.text += " They look like the person on the ID";

    }

    public void Deny()
    {
        if (oAge >= 21  )
        {
            ordertext.text = "That were legal...probably a fake too.";
            strikes++;
            if (strikes == 1)
                imgLife1.enabled = true;

            if (strikes <= 2)
                imgLife2.enabled = true;

            if (strikes <= 3)
                imgLife3.enabled = true;
            Invoke("Reset", 2);

        }
        else
        {
            ordertext.text = "Excellent work!";
            Invoke("Reset", 2);
        }


        if (oFace != true)
        {

            ordertext.text = "That were legal...probably a fake too.";
            strikes++;
            if (strikes == 1)
                imgLife1.enabled = true;

            if (strikes <= 2)
                imgLife2.enabled = true;

            if (strikes <= 3)
                imgLife3.enabled = true;
            Invoke("Reset", 2);
        }
        else
        {
            ordertext.text = "Excellent work!";
            Invoke("Reset", 2);
        }

       
        
    }
    public void Clean()
    {
        ordertext.text += "Thank you.";
        oBar = true;
    }

    public void Brandy()
    {
        brandy += 1;
    }
}



